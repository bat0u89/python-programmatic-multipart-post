import requests
import requests_toolbelt
from requests_toolbelt.multipart.encoder import MultipartEncoder

m = MultipartEncoder(
    fields={'file': ('filename', open('C:\dev\src\python_experiment\data.csv', 'rb'), 'text/plain')}
    )

r = requests.post('http://localhost:8080/handle_form', data=m,
                  headers={'Content-Type': m.content_type})
print(r)