import os
from flask import Flask, request, render_template
import requests

app = Flask(__name__)

@app.route('/handle_form', methods=['POST'])
def handle_form():
    f = request.files['file']
    print("Posted file: {}".format(f))
    print(f.read())
    f.close()
    return ""

@app.route("/")
def index():
    return render_template("index.html");   


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
